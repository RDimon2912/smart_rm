import shutil

from utils.config.config import Config
from utils.helpers.helper import *
from utils.models.item import Item


class Setting(object):

    TRASH_PATH_CONFIG = '~/.trash_config'

    def __init__(self, trash_config_path = TRASH_PATH_CONFIG):
        user_settings = Config.get_config_from_file(trash_config_path).result
        settings = Config.get_config_from_file(
                os.path.join(os.path.dirname(__file__),
                             'defaults.json')).result

        for k in user_settings:
            for k1 in user_settings[k]:
                settings[k][k1] = user_settings[k][k1]

        try:
            self.trash = self.TrashSetting(settings['trash'])
            self.restore = self.RestoreSetting(settings['restore'])
            self.clear = self.ClearSetting(settings['clear'])
            self.show = self.ShowSetting(settings['show'])
        except Exception as e:
            raise SyntaxError('Invalid syntax of trash')
        

    class TrashSetting(object):

        def __init__(self, settings):
            self._path = settings['path']
            self._type_file_info = settings['type_file_info']

        @property
        def path(self):
            return self._path

        @property
        def type_file_info(self):
            return self._type_file_info

    class RestoreSetting(object):

        def __init__(self, settings):
            self._collision_policy = settings['collision_policy']
            if not self._collision_policy in ['make_unique', 'replace']:
                raise Exception("")

        @property
        def collision(self):
            return self._collision_policy

        def check_on_collision(self, item):
            if os.path.exists(item.original_path):
                if self.collision == 'make_unique':
                    item.original_path = get_not_exist_path(item.original_path)
                else:
                    bad_item = Item.make_item_from_path(item.original_path)
                    bad_item.remove()


    class ClearSetting(object):

        def __init__(self, settings):
            self._by_date = parse_date(settings['by_date'])
            self._by_size = parse_size(settings['by_size'])

        @property
        def by_date(self):
            return self._by_date

        @property
        def by_size(self):
            return self._by_size

    class ShowSetting(object):

        def __init__(self, settings):
            self.columns = settings['columns']
            self.size = settings['size']
            if len(self.columns) != len(self.size):
                raise Exception("")

        @property
        def headers(self):
            headers = []
            for i in range(0, len(self.columns)):
                headers.append( (self.columns[i], self.size[i]) )
            return headers