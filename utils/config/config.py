# -*- coding: utf-8 -*-
import ConfigParser
import json
import datetime
import os.path
from utils.helpers.helper import type_config


class Config(object):

    def __init__(self, path):
        self.config_type = 'unknown'
        self.result = {}
        self.config_path = os.path.expanduser(path)

    def init_config_from_json(self):
        with open(self.config_path) as f:
            try:
                self.result = json.load(f)
            except Exception:
                raise SyntaxError("Incorrect type of json file")
        return self.result

    def init_config_from_configfile(self):
        config_parser = ConfigParser.ConfigParser()
        try:
            config_parser.read(self.config_path)
        except Exception:
            raise SyntaxError("Incorrect type of config file")
        for section in config_parser.sections():
            self.result[section] = {}
            for option in config_parser.options(section):
                self.result[section][option] = config_parser.get(section, option)
        return self.result

    @type_config
    def make_configfile(self):
        config = ConfigParser.RawConfigParser()
        for section, options in self.result:
            config.add_section(section)
            for option, value in options:
                config.set(section, option, value)
        with open(self.config_path) as f:
            config.write(f)

    @type_config
    def make_json_file(self):
        with open(self.config_path, 'w') as f:
            json.dump(self.result, f, indent=4)

    @type_config
    def init_config(self):
        if self.config_type == 'json':
            self.init_config_from_json()
        elif self.config_type == 'config':
            self.init_config_from_configfile()
        else: raise Exception("Non-existent type of config")

    @classmethod
    def get_config_from_file(cls, path):
        config = None
        json_cnf = True
        try:
            config = ConfigJson(path)
            config.init_config()
        except SyntaxError:
            json_cnf = False
        if json_cnf:
            return config
        try:
            config = ConfigFile(path)
            config.init_config()
        except SyntaxError:
            raise SyntaxError("Invalid syntax of file")
        return config

    @classmethod
    def make_config(cls, result, type, path):
        if type == 'json':
            cnf = ConfigJson(path)
            cnf.result = result
            cnf.make_json_file()
        elif type == 'config':
            cnf = ConfigFile(path)
            cnf.result = result
            cnf.make_configfile()
        else:
            raise TypeError('Incorrect type of config')

class ConfigJson(Config):
    def __init__(self, path):
        super(ConfigJson, self).__init__(path)
        self.config_type = "json"


class ConfigFile(Config):
    def __init__(self, path):
        super(ConfigFile, self).__init__(path)
        self.config_type = "config"
