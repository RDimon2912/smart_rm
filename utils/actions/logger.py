import logging

class Logger(object):

    def __init__(self, silence = True, verbose = False):
        self.log = self.create_logger()
        if silence:
            self.silence()
        elif verbose:
            self.verbose()

    def create_logger(self):
        log = logging.getLogger()
        log.setLevel(logging.WARNING)
        hdlr = logging.StreamHandler()
        hdlr.setLevel(logging.DEBUG)
        fmt = logging.Formatter('%(message)s')
        hdlr.setFormatter(fmt)
        log.addHandler(hdlr)
        return log

    def verbose(self):
        self.log.setLevel(logging.INFO)

    def silence(self):
        self.log.setLevel(logging.FATAL)

    def add_info(self, msg):
        self.log.info(msg)

    def add_error(self, msg):
        self.log.error(msg)

    def add_debug(self, msg):
        self.log.debug(msg)
