import re

import utils.helpers.helper as helper
from utils.actions.logger import Logger
from utils.actions.modes import Mode
from utils.config.settings import Setting
from utils.models.remover import Remover
from utils.models.trash import Trash


class Action(object):

    def __init__(self, args):
        self.args = args
        self.logger = Logger(args.silence, args.verbose)
        self.settings = Setting() if not args.trash else \
            Setting(args.trash)
        self.trash = Trash(self.settings, self.logger, self.args.dry_mode)
        self.trash.automatic_clear()
        getattr(self, args.func)()


    def remove(self):
        remover = Remover(self.trash,
                               Mode.get_mode_from_args(self.args),
                               self.logger)

        if self.args.pattern:
            helper.update_file_list_by_pattern(self.args)
        for cur_path in self.args.file:
            try:
                if self.args.recursive:
                    remover.remove_recursive(cur_path)
                elif self.args.dir:
                    remover.remove_empty_dir(cur_path)
                else:
                    remover.remove_file(cur_path)
            except Exception as e:
                self.logger.add_error(str(e))

    def restore(self):
        self._update_file_list_by_pattern()
        for cur_path in self.args.file:
            try:
                item = self.trash.create_item_in_trash(cur_path)
                self.trash.restore(item)
            except Exception as e:
                self.logger.add_error(str(e))

    def show(self):
        self.trash.show()

    def clear(self):
        self._update_file_list_by_pattern()
        for cur_path in self.args.file:
            try:
                item = self.trash.create_item_in_trash(cur_path)
                self.trash.clear(item)
            except Exception as e:
                self.logger.add_error(str(e))

    def _update_file_list_by_pattern(self):
        if self.args.all:
            self.args.pattern = ''
        if self.args.pattern is not None:
            self.args.file = []
            for item, conf in self.trash.files:
                if re.search(self.args.pattern, item.name):
                    self.args.file.append(item.name)


