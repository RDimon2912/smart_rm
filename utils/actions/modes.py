import os

from utils.helpers.helper import query


class Mode(object):

    def __init__(self):
        self.type = 'default'

    def check_to_exist(self, path):
        if not os.path.lexists(path):
            raise OSError('{0}: Invalid path'.format(path))

    def can_i_remove(self, path):
        self.check_to_exist(path)
        if not os.access(path, os.W_OK):
            return query('{0}: Do you want to remove protected item? '.format(path))
        return True

    def can_i_go_down(self, path):
        self.check_to_exist(path)
        if not os.access(path, os.W_OK):
            return query('{0}: Do you want to go down in protected item? '.format(path))
        return True

    @classmethod
    def get_mode_from_args(cls, arg):
        if arg.interactive:
            return Interactive()
        elif arg.force:
            return Force()
        else:
            return Mode()


class Force(Mode):

    def __init__(self):
        super(Force, self).__init__()
        self.type = 'force'

    def can_i_remove(self, path):
        self.check_to_exist(path)
        return True

    def can_i_go_down(self, path):
        return self.can_i_remove(path)


class Interactive(Mode):

    def __init__(self):
        super(Interactive, self).__init__()
        self.type = 'interactive'

    def can_i_remove(self, path):
        self.check_to_exist(path)
        if not os.access(path, os.W_OK):
            return query('{0}: Do you want to remove protected item? '.format(path))
        else:
            return query('{0}: Do you want to remove this item? '.format(path))


    def can_i_go_down(self, path):
        self.check_to_exist(path)
        if not os.access(path, os.W_OK):
            return query('{0}: Do you want to go down in protected item? '.format(path))
        else:
            return query('{0}: Do you want to go down in this item? '.format(path))