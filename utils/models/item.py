import os.path
import shutil

from utils.config.config import *
from utils.helpers.helper import *


class Item(object):

    def __init__(self, path, remote = False):
        self.original_path = os.path.abspath(path)
        self.current_path = os.path.abspath(path)
        self.name = os.path.basename(self.current_path)
        self.remote = remote
        self._type = 'unknown'
        self._size = os.path.getsize(self.current_path)
        self._info = {}

    def remove(self):
        os.remove(self.current_path)

    def move(self, new_path, dry_mode):
        if dry_mode:
            return
        new_path = os.path.abspath(new_path)
        os.rename(self.current_path, new_path)
        self.current_path = new_path
        self.remote = True

    @property
    def size(self):
        return self._size

    @property
    def type(self):
        return self._type

    @property
    def info(self):
        return self._info

    @info.setter
    def info(self, hash):
        self._info = hash

    def init_info_about_item(self):
        self.info['original-path'] = self.original_path
        self.info['name'] = self.name
        self.info['type'] = self.type
        self.info['size'] = self.size
        self.info['time'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    @classmethod
    def make_item_from_path(cls, path, remote = False):
        path = os.path.abspath(path)
        if not os.path.lexists(path):
            raise OSError("{0}: File not found".format(path))
        if os.path.islink(path):
            return Link(path, remote)
        elif os.path.isfile(path):
            return File(path, remote)
        elif os.path.isdir(path):
            return Directory(path, remote)
        else:
            raise OSError("Unknown items type")

class Directory(Item):

    def __init__(self, path, remote = False):
        super(Directory, self).__init__(path, remote)
        self._type = "directory"
        self._set_size()
        self.init_info_about_item()

    def _set_size(self):
        self._size = os.path.getsize(self.current_path)

        def _walker(arg, dirname, names):
            for name in names:
                self._size += os.path.getsize(os.path.join(dirname, name))
        os.path.walk(self.current_path, _walker, self._size)


    def remove(self):
        shutil.rmtree(self.current_path)

    def move(self, new_path, dry_mode):
        if dry_mode:
            return
        shutil.move(self.current_path, new_path)

    def listdir(self):
        f = lambda(path): \
            Item.make_item_from_path(os.path.join(self.current_path, path), self.remote)
        items = os.listdir(self.current_path)
        return map(f, items)

class File(Item):

    def __init__(self, path, remote = False):
        super(File, self).__init__(path, remote)
        self._type = "file"
        self.init_info_about_item()


class Link(Item):

    def __init__(self, path, remote = False):
        super(Link, self).__init__(path, remote)
        self._type = "link"
        self.init_info_about_item()
