from utils.helpers.helper import *
from utils.models.item import *


class Remover(object):

    def __init__(self, trash, mode, logger):
        self.trash = trash
        self.mode = mode
        self.logger = logger

    def remove_file(self, path):
        if not self.mode.can_i_remove(path):
            return
        item = Item.make_item_from_path(path)
        if item.type == 'directory':
            raise OSError("{0}: It is directory".format(path))
        self.trash.add_item_to_trash(item, item.move)

    def remove_empty_dir(self, path):
        if not self.mode.can_i_remove(path):
            return
        item = Item.make_item_from_path(path)

        if not item.type == 'directory':
            raise OSError("{0}: It is not directory".format(path))
        if len(item.listdir()) != 0:
            raise OSError("{0}: Directory is not empty")
        self.trash.add_item_to_trash(item, item.move)

    def _remove_iteration(self, item, new_path, dry_mode):
        if item.type == 'directory':
            path = get_not_exist_path(new_path)
            if not dry_mode:
                os.makedirs(path)
                shutil.copystat(item.current_path, path)
        else:
            if not self.mode.can_i_remove(item.current_path):
                return
            path = get_not_exist_path(new_path)
            item.move(path, dry_mode)
        self.logger.add_info('{0}: removed'.format(item.original_path))

    def _dfs_remove(self, item, path, dry_mode):
        self._remove_iteration(item, path, dry_mode)
        if item.type == 'directory':
            if not self.mode.can_i_go_down(item.current_path):
                return
            array = item.listdir()
            cnt = len(array)
            for next_item in array:
                try:
                    self._dfs_remove(next_item, os.path.join(path, next_item.name), dry_mode)
                    cnt -= 1
                except Exception as e:
                    self.logger.add_error(str(e))
            if not self.mode.can_i_remove(item.current_path):
                return
            if cnt != 0:
                raise OSError("{0}: Directory is not empty".format(item.current_path))
            else:
                if not dry_mode:
                    item.remove()
                self.logger.add_info('{0}: removed'.format(item.original_path))

    def remove_recursive(self, path):
        item = Item.make_item_from_path(path)
        if not item.type == 'directory':
            return self.remove_file(path)

        def func_move(new_path, dry_mode):
            self._dfs_remove(item, new_path, dry_mode)

        self.trash.add_item_to_trash(item, func_move)