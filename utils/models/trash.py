import os.path
from datetime import datetime

from utils.helpers.helper import *
from utils.models.item import *
from utils.models.window import Window


class Trash(object):

    def __init__(self, settings, logger, dry_mode):
        self.dry_mode = dry_mode
        self.logger = logger
        self.settings = settings
        self.current_path = os.path.expanduser(self.settings.trash.path)
        self.files_path = os.path.join(self.current_path, 'files')
        self.files_info_path = os.path.join(self.current_path, 'info')
        self.type_file_info = str(self.settings.trash.type_file_info)
        self.files = []
        self._create_if_not_exist_folders()

    def indexing_of_trash(self):
        self.files = []
        files_dir = Directory(self.files_path, True).listdir()
        files_info_dir = os.listdir(self.files_info_path)
        for file in files_dir:
            cnf_name = file.name + '.' + self.type_file_info
            if files_info_dir.count(cnf_name) == 0:
                continue
            path_config = os.path.join(self.files_info_path, file.name) + '.' + self.type_file_info
            cnf = Config.get_config_from_file(path_config)
            file.original_path = cnf.result['trash-info']['original-path']
            file.info = cnf.result
            self.files.append((file, cnf.result['trash-info']))

    def create_item_in_trash(self, path):
        path = os.path.join(self.files_path, path)
        item = Item.make_item_from_path(path)
        path_cnf = os.path.join(self.files_info_path, item.name) + '.' + self.type_file_info
        cnf = Config.get_config_from_file(path_cnf)
        item.original_path = cnf.result['trash-info']['original-path']
        item.info = cnf.result
        return item

    def get_size_of_trash(self):
        return Directory(self.files_path, True).size

    def add_item_to_trash(self, item, func_move):
        new_path = os.path.join(self.files_path, item.name)
        new_path = get_not_exist_path(new_path)

        func_move(new_path, self.dry_mode)
        cnf = { 'trash-info' : item.info }
        path_info = os.path.join(self.files_info_path, os.path.basename(new_path) + '.' + self.type_file_info)
        if not self.dry_mode:
            Config.make_config(cnf, self.type_file_info, path_info)
        self.logger.add_info('{0}: was added to trash'.format(item.original_path))

    def _create_if_not_exist_folders(self):
        create_path_if_not_exist(self.current_path)
        create_path_if_not_exist(self.files_path)
        create_path_if_not_exist(self.files_info_path)

    def _get_array_list(self):
        self.indexing_of_trash()
        new_arr = []
        for i in range(0, len(self.files)):
            pre_arr = []
            for column in self.settings.show.columns:
                pre_arr.append(self.files[i][1][column])
            new_arr.append(pre_arr)
        return new_arr

    def clear(self, item):
        conf_item = os.path.join(self.files_info_path,
                                 os.path.basename(item.current_path) + '.' + self.type_file_info)
        conf_item = Item.make_item_from_path(conf_item, True)
        if not self.dry_mode:
            item.remove()
            conf_item.remove()
        self.logger.add_info('{0}: deleted.'.format(item.current_path))

    def restore(self, item):

        conf_item = os.path.join(self.files_info_path,
                                 os.path.basename(item.current_path) + '.' + self.type_file_info)
        conf_item = Item.make_item_from_path(conf_item, True)
        create_path_if_not_exist(os.path.dirname(item.original_path))
        self.settings.restore.check_on_collision(item)
        if not self.dry_mode:
            item.move(item.original_path, self.dry_mode)
            conf_item.remove()
        self.logger.add_info('{0}: restored.'.format(item.current_path))

    def automatic_clear(self):
        self.logger.add_info("automatic cleaning is started")
        self.indexing_of_trash()
        timedelta = self.settings.clear.by_date
        max_size = self.settings.clear.by_size
        now = datetime.datetime.now()
        for file, cnf in self.files:

            if (max_size == 'off') and (max_size < cnf['size']):
                self.clear(file)
            if timedelta != 'off':
                deletion_date = datetime.datetime.strptime(cnf["time"],
                                                           "%Y-%m-%d %H:%M:%S")
                if now - deletion_date > timedelta:
                    self.clear(file)


    def show(self):
        array = self._get_array_list()
        headers = self.settings.show.headers
        win = Window(headers, array)
        while True:
            res = win.show()
            if res == 'quit':
                break
            elif res == 'delete':
                for pos in win.selected_elements:
                    item, cnf = self.files[pos]
                    self.clear(item)
                array = self._get_array_list()
                win.array_of_elements = array
                win.selected_elements.clear()
            elif res == 'enter':
                for pos in win.selected_elements:
                    item, cnf = self.files[pos]
                    self.restore(item)
                array = self._get_array_list()
                win.array_of_elements = array
                win.selected_elements.clear()
