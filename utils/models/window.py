import curses
import os

class Window(object):

    def __init__(self, headers, array):
        self.count_columns = len(headers)
        self.table_header = headers
        self.array_of_elements = array
        self.selected_elements = set()
        self.status_bar_str = "Press 'q' to exit"

    def _add_status_bar(self, stdscr, key):
        height, width = stdscr.getmaxyx()
        stdscr.attron(curses.color_pair(3))
        str_status = '{0:{width}}'.format(self.status_bar_str + str(key), width = width)
        stdscr.addnstr(height - 1, 0, str_status, width - 1)
        stdscr.attroff(curses.color_pair(3))

    def _add_table_header(self, stdscr):
        height, width = stdscr.getmaxyx()
        cur_pos = 0
        stdscr.attron(curses.color_pair(1))
        for name, size in self.table_header:
            if cur_pos >= width - 1: continue
            stdscr.addnstr(0, cur_pos, '{0:{width}}'.format(name, width = size),
                           width - cur_pos)
            cur_pos += size

        if cur_pos < width:
            stdscr.addnstr(0, cur_pos, '{0:{width}}'.format('', width = width), width - cur_pos )
        stdscr.attroff(curses.color_pair(1))

    def _add_elem(self, stdscr, cur_pos, i, j, x, y):
        height, width = stdscr.getmaxyx()
        if x >= width - 1: return
        if cur_pos == i:
            stdscr.attron(curses.color_pair(2))
        elif i in self.selected_elements:
            stdscr.attron(curses.color_pair(4))

        name, size = self.table_header[j]
        if j == self.count_columns - 1: size = width - x
        str = '{0:{width}}'.format(self.array_of_elements[i][j],
                                   width=size)

        stdscr.addnstr(y, x, str, min(width - x, size))

        if cur_pos == i:
            stdscr.attroff(curses.color_pair(2))
        elif i in self.selected_elements:
            stdscr.attroff(curses.color_pair(4))

    def _swap_selected(self, pos):
        if pos in self.selected_elements:
            self.selected_elements.remove(pos)
        else:
            self.selected_elements.add(pos)

    def _add_message_about_empty(self, stdscr):
        height, width = stdscr.getmaxyx()
        str = 'Trash is empty.'
        str = '{0:{align}{width}}'.format(str, align='^', width = width)
        stdscr.addnstr(1, 0, str, width)

    def _wrap_function(self, stdscr):
        cursor = 1
        up_pos = 0
        key = 0
        ln = len(self.array_of_elements)
        while True:
            stdscr.clear()
            height, width = stdscr.getmaxyx()

            self._add_status_bar(stdscr, key)
            self._add_table_header(stdscr)

            if ln == 0:
                self._add_message_about_empty(stdscr)
            for i in range(up_pos, up_pos + min (height - 2, ln)):
                cur_x = 0
                for j in range(0, self.count_columns):
                    self._add_elem(stdscr, cursor + up_pos - 1, i, j, cur_x, i - up_pos + 1)
                    cur_x += self.table_header[j][1]

            stdscr.refresh()
            key = stdscr.getch()

            if key == curses.KEY_DOWN or key == curses.KEY_SF or key == 66:
                if key == curses.KEY_SF or key == 66:
                    self._swap_selected(cursor + up_pos - 1)
                if cursor + up_pos == ln: continue
                if cursor == height - 2:
                    up_pos += 1
                else: cursor += 1

            elif key == curses.KEY_UP or key == curses.KEY_SR or key == 65:
                if key == curses.KEY_SR or key == 65:
                    self._swap_selected(cursor + up_pos - 1)
                if cursor + up_pos == 1: continue
                if cursor == 1:
                    up_pos -= 1
                else: cursor -= 1
            elif key == 113:
                return 'quit'
            elif key == 10:
                if not cursor + up_pos - 1 in self.selected_elements:
                    self._swap_selected(cursor + up_pos - 1)
                return 'enter'
            elif key == curses.KEY_DC:
                if not cursor + up_pos - 1 in self.selected_elements:
                    self._swap_selected(cursor + up_pos - 1)
                return 'delete'

    def show(self):
        try:
            stdscr = curses.initscr()
            # curses.noecho()
            curses.cbreak()
            stdscr.keypad(1)

            try:
                curses.start_color()
            except:
                pass
            curses.use_default_colors()
            curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)
            curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_CYAN)
            curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)
            curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_YELLOW)

            res = self._wrap_function(stdscr)
        finally:
            # curses.echo()
            curses.nocbreak()
            curses.endwin()
        return res


# headers = [('Name', 27), ('Full path', 10)]
# path = os.path.expanduser('~/')
# arr = os.listdir(path)
# new_arr = []
# for item in arr:
#     new_arr.append([item, os.path.join(path, item)])
#
# win = Window(headers, new_arr)
# while True:
#     res = win.show()
#     if res == 'quit':
#         break
#     sub_arr = []
#     for i in range(0, len(new_arr)):
#         if not i in win.selected_elements:
#             sub_arr.append(new_arr[i])
#     new_arr = sub_arr
#     win.array_of_elements = new_arr
#     win.selected_elements.clear()
#
