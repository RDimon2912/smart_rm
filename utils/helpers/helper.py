import os
import os.path
import re
import datetime

def is_directory(func):
    def new_func(obj):
        if not obj.is_dir:
            raise Exception("Object is not directory!")
        else: return func(obj)
    return new_func

def check_config(func):
    def new_func(obj, conf):
        if not conf.has_key('trash-path'):
            raise Exception('Trash path is not exist!')
        else: return func(obj, conf)
    return new_func

def type_config(func):
    def new_func(self):
        if self.config_type == 'unknown':
            raise OSError('smart_rm: config type is unknown')
        else: return func(self)
    return new_func

def create_path_if_not_exist(path):
    if not os.path.lexists(path):
        os.makedirs(path)

def get_not_exist_path(path):
    if not os.path.exists(path):
        return path
    i = 1
    while True:
        new_path = path + "." + i.__str__()
        if not os.path.exists(new_path):
            return new_path
        i += 1

def query(message):
    answer = raw_input(message)
    if answer.lower() in ['y', 'yes']:
        return True
    else: return False

def parse_size(str):
    if str == "off":
        return "off"
    try:
        size, exp = str.split('.')
        size = int(size)
    except Exception:
        raise Exception("config: incorrect size")
    if exp == "kb":
        size *= 1024
    elif exp == "mb":
        size *= 1024 ** 2
    elif exp == "b":
        pass
    else:
        raise Exception("config: incorrect dimension size")
    return size

def parse_date(timedelta):
    if timedelta == "off":
        return "off"
    try:
        delta, exp = timedelta.split('.')
    except Exception:
        raise Exception("config: incorrect config file")
    delta = int(delta)
    if exp == "y":
        delta = datetime.timedelta(days=365 * delta)
    elif exp == "m":
        delta = datetime.timedelta(days=30 * delta)
    elif exp == "d":
        delta = datetime.timedelta(days=delta)
    elif exp == "H":
        delta = datetime.timedelta(hours=delta)
    elif exp == "M":
        delta = datetime.timedelta(minutes=delta)
    elif exp == "S":
        delta = datetime.timedelta(seconds=delta)
    else:
        raise Exception("config: incorrect dimension timedelta")
    return delta

def _grep(path, pattern):
    remove_list = []
    def _walker(arg, dirname, names):
        for name in names:
            if re.search(pattern, name):
                remove_list.append(os.path.join(dirname, name))
    os.path.walk(path, _walker, remove_list)
    return remove_list

def update_file_list_by_pattern(args, current_path = None):
    if not current_path:
        current_path = os.getcwd()
    remove_list = []
    if args.pattern:
        for pattern in args.pattern:
            remove_list.extend(_grep(current_path, pattern))
    args.file = remove_list
    args.pattern = False
