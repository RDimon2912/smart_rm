# -*- coding: utf-8 -*-
import argparse

from utils.models.remover import Remover
from utils.models.trash import Trash

def parser_trash():

    parser = argparse.ArgumentParser(prog='smart_rm')


    subparsers = parser.add_subparsers()

    parser_rm = subparsers.add_parser('rm', usage='%(prog)s [options] .. [file] ..',
                                      help='Добавить в корзину')
    parser_rm.add_argument('file', nargs='+', type=str, help='путь/пути к файлу/файлам')
    force_group = parser_rm.add_mutually_exclusive_group()
    force_group.add_argument('-f', '--force', action='store_true',
                             help='игнорировать несуществующие файлы и аргументы, ни о чем не спрашивать')
    force_group.add_argument('-i', '--interactive', action='store_true',
                             help='запрашивать подтверждение перед каждым удалением')
    parser_rm.add_argument('-r', '-R', '--recursive', action='store_true',
                           help='рекурсивно удалять каталоги и их содержимое')
    parser_rm.add_argument('-d', '--dir', action='store_true', help='удалять пустые каталоги')
    parser_rm.set_defaults(func='remove')

    parser_show = subparsers.add_parser('show', usage='%(prog)s',
                                        help='Показать корзину')
    parser_show.set_defaults(func='show')

    parser_clear = subparsers.add_parser('clear', usage='%(prog)s [options] .. [file] ..',
                                         help='''Очистить из корзины файлы''')
    parser_clear.add_argument('--all', action='store_true', help='очистить корзину полностью')
    parser_clear.add_argument('file', nargs='*', type=str, help='путь/пути к файлу/файлам')
    parser_clear.set_defaults(func = 'clear')

    parser_restore = subparsers.add_parser('restore', usage='%(prog)s [options] .. [file] ..',
                                           help='''Восстановить из корзины файлы''')
    parser_restore.add_argument('--all', action='store_true', help='очистить корзину полностью')
    parser_restore.add_argument('file', nargs='*', type=str, help='путь/пути к файлу/файлам')
    parser_restore.set_defaults(func = 'restore')


    parser.add_argument('--trash', type=str, default=None, help='путь к корзине')
    force_group = parser.add_mutually_exclusive_group()
    force_group.add_argument('-v', '--verbose', action='store_true', help='пояснять производимые действия')
    force_group.add_argument('-s', '--silence', action='store_true', help='режим тишины')
    parser.add_argument('--dry-mode', action='store_true', help='режим попробовать')
    parser.add_argument('--pattern', type=str, default=None, help='действие по шаблону')

    return parser.parse_args()
