#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
import sys
import os
import unittest
import tempfile
import shutil
from utils.actions.logger import Logger
from utils.actions.modes import Force
from utils.config.config import Config
from utils.config.settings import Setting
from utils.models.item import Item
from utils.models.remover import Remover
from utils.models.trash import Trash


class Test(unittest.TestCase):
    def setUp(self):
        self.trash_path = tempfile.mkdtemp()
        self.logger = Logger()
        self.settings = Setting()
        self.settings.trash._path = self.trash_path
        self.trash = Trash(self.settings, self.logger, False)
        self.remover = Remover(self.trash, Force(), self.logger)

    def tearDown(self):
        shutil.rmtree(self.trash_path)

    def test_make_dir(self):
        test_path = self.trash.current_path
        self.assertTrue(os.path.exists(test_path))

    def test_remove(self):
        file_1 = tempfile.NamedTemporaryFile()
        file_2 = tempfile.NamedTemporaryFile()
        
        for f in [file_1, file_2]:
            self.remover.remove_file(f.name)

        self.assertFalse(os.path.exists(file_1.name), 'File remove is not working')
        self.assertFalse(os.path.exists(file_2.name), 'File remove is not working')

    def test_rmdir(self):

        dir_path_1 = tempfile.mkdtemp()
        dir_path_2 = tempfile.mkdtemp()

        for f in [dir_path_1, dir_path_2]:
            self.remover.remove_recursive(f)

        self.assertFalse(os.path.exists(dir_path_1), 'Directory remove is not working')
        self.assertFalse(os.path.exists(dir_path_2), 'Directory remove is not working')

    def test_remove_and_restore(self):
        dir_path = tempfile.mkdtemp()
        file_in_dir_1 = tempfile.NamedTemporaryFile(dir=dir_path)
        file_in_dir_2 = tempfile.NamedTemporaryFile(dir=dir_path)

        self.remover.remove_recursive(dir_path)
        # print file_in_dir_1
        self.assertFalse(os.path.exists(file_in_dir_1.name), 'The file was not removed')
        self.assertFalse(os.path.exists(file_in_dir_2.name), 'The file was not removed')

        item1 = self.trash.create_item_in_trash(os.path.basename(dir_path))
        self.trash.restore(item1)
        self.assertTrue(os.path.exists(file_in_dir_1.name), 'The file was not restored')
        self.assertTrue(os.path.exists(file_in_dir_2.name), 'The file was not restored')

    def test_clear_trash(self):
        file_1 = tempfile.NamedTemporaryFile()
        file_2 = tempfile.NamedTemporaryFile()

        # print(file_1.name)
        # print(file_2.name)

        for f in [file_1, file_2]:
            self.remover.remove_file(f.name)

        item1 = self.trash.create_item_in_trash(os.path.basename(file_1.name))
        item2 = self.trash.create_item_in_trash(os.path.basename(file_2.name))
        self.trash.clear(item1)
        self.trash.clear(item2)

        self.assertTrue(len(os.listdir(self.trash.files_path)) == 0)
