from setuptools import setup, find_packages
import sys

setup(name = "smart_rm",
      version='0.1',
      description="Python smart rm",
      author="Dmitry Rozhkov",
      author_email="rdimon2912@gmail.com",
      packages=find_packages(),
      include_package_data=True,
      entry_points={
          'console_scripts':['smart_rm = utils.main:main']
    }
    )
